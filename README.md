# HA Maxscale

* This was meant to add a Maxscale SQL proxy to an existing cluster.
* For vagrant and testing purposes I've included a mariadb cluster setup.

# Run

## Ansible dependencies

* These are all my own roles hosted on [Gitlab.com](https://gitlab.com/stemid-ansible/roles).

```
$ ansible-galaxy install -r requirements.yml
```

## Vagrant up

* By default vagrant only brings up two maxscale nodes.
* Prefix ``UP_MARIADB=True`` to also bring a sample MariaDB cluster up that Maxscale can proxy connections to.

```
$ UP_MARIADB=True vagrant up --no-provision
$ UP_MARIADB=True vagrant ssh-config > ~/.ssh/vagrant.conf
```

## Prepare OS

    $ ansible-playbook -i inventory/default/hosts bootstrap.yml

## Bootstrap MariaDB primary server

* AFAIK this only works with one primary because of how it uses galera to bootstrap.
* Skip this and next step if you have an existing mariadb cluster.

```
$ ansible-playbook -i inventory/default/hosts mariadb.yml -e 'bootstrap_mariadb_primary=True'
```

### Warning: If this step should fail somewhere

Because of how ``galera_new_cluster`` starts mariadb it is not recommended to restart it using systemctl before at least one standby node has joined, because it will attempt to request state from another node in the gcomm address list and will not succeed.

My recommendation is to start over completely and re-run the step, before you move on to the joining the standby nodes.

    ansible-playbook -i inventory/default/hosts uninstall.yml

>**Warning** The above playbook will delete your datadir.

## Join standby nodes or setup arbitrator

* Do this when adding new standby nodes. Just remember to shutdown garb if you're using an old arbitrator as a standby node.

```
$ ansible-playbook -i inventory/default/hosts mariadb.yml
```

## Setup maxscale

    $ ansible-playbook -i inventory/default/hosts maxscale.yml

# Notes

* Only supports MariaDB 10.1 to 10.3 on Debian due to [package names changing](https://mariadb.com/kb/en/installing-mariadb-deb-files/#installing-mariadb-galera-cluster-with-apt).
* Uses mariabackup instead of xtrabackup as SST method because xtrabackup is deprecated in 10.3.
